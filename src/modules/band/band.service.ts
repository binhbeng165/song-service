import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import { CreateBandDto } from './dto/create-band.dto';
import { UpdateBandDto } from './dto/update-band.dto';
import { Band } from '@prisma/client';
import { ClientProxy } from '@nestjs/microservices';
import { PrismaService } from 'src/database/prisma.service';

@Injectable()
export class BandService {

  constructor(
    // @Inject('AUTH_SERVICE') private readonly authClient: ClientProxy,
    private readonly prisma: PrismaService,
    // private readonly redisService: RedisService,
  ) {
    // this.authClient.connect();
  }
  async createNewBand(createBandDto: CreateBandDto): Promise<Band> {
    try {
      const band = await this.prisma.band.create({ data: createBandDto });
      return band;
    } catch (e) {
      throw new BadRequestException(e);
    }
  }
  findAll() {
    return `This action returns all band`;
  }
}
