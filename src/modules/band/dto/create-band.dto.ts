import { ApiProperty } from "@nestjs/swagger";
import { IsArray, IsNotEmpty, IsOptional, IsString } from "class-validator";

export class CreateBandDto {
    @ApiProperty()
    @IsString()
    @IsNotEmpty({ message: 'name is required' })
    public name: string;
  
    @ApiProperty()
    @IsArray()
    public artistIds: number[];
  
    @ApiProperty()
    @IsString()
    @IsOptional()
    public fileId: string;
}
