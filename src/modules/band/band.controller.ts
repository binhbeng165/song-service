import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { BandService } from './band.service';
import { CreateBandDto } from './dto/create-band.dto';
import { UpdateBandDto } from './dto/update-band.dto';
import { MessagePattern } from '@nestjs/microservices';

@Controller('band')
export class BandController {
  constructor(private readonly bandService: BandService) { }

  @MessagePattern('band.create')
  create(@Body() createBandDto: CreateBandDto) {
    return this.bandService.createNewBand(createBandDto);
  }
  @MessagePattern('band.findAll')
  findAll() {
    return this.bandService.findAll();
  }
}
