import { IsOptional, IsString, IsBoolean } from 'class-validator';

export class CreateArtistDto {
  @IsOptional()
  id?: number;

  @IsString()
  name: string;

  @IsString()
  bio: string;

}