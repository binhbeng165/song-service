import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateArtistDto } from './dto/create-artist.dto';
import { UpdateArtistDto } from './dto/update-artist.dto';
import { PrismaService } from 'src/database/prisma.service';

@Injectable()
export class ArtistService {
  constructor(
    // @Inject('AUTH_SERVICE') private readonly authClient: ClientProxy,
    private readonly prisma: PrismaService,
    // private readonly redisService: RedisService,
  ) {
    // this.authClient.connect();
  }
  async createNewArtist(createArtistDto: CreateArtistDto) {
    try {
      const artist = await this.prisma.artist.create({ data: createArtistDto });
      return artist;
    } catch (e) {
      throw new BadRequestException(e);
    }
  }

  findAll() {
    return `This action returns all artist`;
  }

  findOne(id: number) {
    return `This action returns a #${id} artist`;
  }

  update(id: number, updateArtistDto: UpdateArtistDto) {
    return `This action updates a #${id} artist`;
  }

  remove(id: number) {
    return `This action removes a #${id} artist`;
  }
}
