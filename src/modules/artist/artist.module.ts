import { Module } from '@nestjs/common';
import { ArtistService } from './artist.service';
import { ArtistController } from './artist.controller';
import { PrismaModule } from 'src/database/prisma.module';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { EnvService } from 'src/configurations/env.service';

@Module({
  imports: [PrismaModule,   
     ClientsModule.registerAsync([
    {
      name: 'AUTH_SERVICE',
      useFactory: async (env: EnvService) => ({
        transport: Transport.RMQ,
        options: {
          urls: [`${env.get('RABBITMQ.URL')}`],
          queue: `${env.get('RABBITMQ.SERVICE.AUTH')}`,
          queueOptions: {
            durable: false,
          },
        },
      }),
      inject: [EnvService],
    },
  ]),],
  controllers: [ArtistController],
  providers: [ArtistService]
})
export class ArtistModule { }
