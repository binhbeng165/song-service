import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { ArtistService } from './artist.service';
import { CreateArtistDto } from './dto/create-artist.dto';
import { UpdateArtistDto } from './dto/update-artist.dto';

@Controller()
export class ArtistController {
  constructor(private readonly artistService: ArtistService) {}

  @MessagePattern('artist.create')
  create(@Payload() createArtistDto: CreateArtistDto) {
    return this.artistService.createNewArtist(createArtistDto);
  }

  @MessagePattern('findAllArtist')
  findAll() {
    return this.artistService.findAll();
  }

  @MessagePattern('findOneArtist')
  findOne(@Payload() id: number) {
    return this.artistService.findOne(id);
  }

  @MessagePattern('updateArtist')
  update(@Payload() updateArtistDto: UpdateArtistDto) {
    return this.artistService.update(updateArtistDto.id, updateArtistDto);
  }

  @MessagePattern('removeArtist')
  remove(@Payload() id: number) {
    return this.artistService.remove(id);
  }
}
