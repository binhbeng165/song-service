import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class CreateRequestLogDto {
    @ApiProperty()
    @IsString()
    @IsNotEmpty({ message: 'url is required' })
    public url: string;

    @ApiProperty()
    @IsString()
    public statusCode: number;

    @ApiProperty()
    @IsOptional()
    @IsString()
    public method?: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty({ message: 'req is required' })
    public req: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty({ message: 'res is required' })
    public res: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty({ message: 'res is required' })
    public timestamp: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty({ message: 'res is required' })
    public headers: string;

    @ApiProperty()
    @IsOptional()
    @IsString()
    public dur?: number;
}
