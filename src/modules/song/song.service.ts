import { Injectable } from '@nestjs/common';
import { CreateSongDto } from './dto/create-song.dto';
import { UpdateSongDto } from './dto/update-song.dto';
import { PrismaService } from 'src/database/prisma.service';

@Injectable()
export class SongService {

  constructor(
    // @Inject('AUTH_SERVICE') private readonly authClient: ClientProxy,
    private readonly prisma: PrismaService,
    // private readonly redisService: RedisService,
  ) {
    // this.authClient.connect();
  }
  async create(createSongDto: CreateSongDto) {
    try {
      const { name, lyrics, image, releaseYear, chords, composerIds, artistIds, bandIds } = createSongDto;
      const song = await this.prisma.song.create({
        data: {
          name,
          lyrics,
          image,
          releaseYear,
          chords,
          composers: { connect: composerIds?.map(id => ({ id })) },
          artists: { connect: artistIds?.map(id => ({ id })) },
          bands: { connect: bandIds?.map(id => ({ id })) },
        },
      });
      return song;
    } catch (error) {
      throw error
    }

  }

  async findAll() {
    try {
      const data = await this.prisma.song.findMany({
        include: {
          composers: true,
          artists: true,
          bands: true
        },
      })
      return data;
    } catch (e) {
      throw e;
    }
  }

  findOne(id: number) {
    return `This action returns a #${id} song`;
  }

  update(id: number, updateSongDto: UpdateSongDto) {
    return `This action updates a #${id} song`;
  }

  remove(id: number) {
    return `This action removes a #${id} song`;
  }
}
