import { ApiProperty } from "@nestjs/swagger";
import { IsArray, IsEmpty, IsNotEmpty, IsOptional, IsString } from "class-validator";

export class CreateSongDto {

    @ApiProperty()
    @IsString()
    @IsNotEmpty({ message: 'name is required' })
    public name: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty({ message: 'lyrics is required' })
    public lyrics: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    public releaseYear: string;

    @ApiProperty()
    @IsString()
    @IsOptional()
    public image: string;

    @IsArray()
    @IsString({ each: true })
    chords: string[];
  
    @IsArray()
    // @IsOptional()
    composerIds?: number[];
  
    @IsArray()
    @IsOptional()
    artistIds?: number[];
  
    @IsArray()
    @IsOptional()
    bandIds?: number[];

    @IsEmpty({ message: 'Either artistIds or bandIds must be provided' })
    emptyCheck?: null;
}
