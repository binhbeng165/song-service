import { Controller, Get, Post } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { SongService } from './song.service';
import { CreateSongDto } from './dto/create-song.dto';

@Controller("song")
export class SongController {
  constructor(private readonly songService: SongService) { }

  @MessagePattern('song.create')
  create(@Payload() createSongDto: CreateSongDto, requestId?: string) {
    return this.songService.create(createSongDto);
  }

  @Get()
  findAll() {
    return this.songService.findAll();
  }

}
