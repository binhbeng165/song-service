import { Controller, Get } from '@nestjs/common';
import { Public } from './core';
import { HealthCheck, HealthCheckService } from '@nestjs/terminus';
import { PrismaService } from 'src/database/prisma.service';

@Controller()
export class AppController {
  constructor(
    private healthCheckService: HealthCheckService,
    private prismaService: PrismaService,
  ) {}

  @Get('/health')
  @HealthCheck()
  @Public()
  public async getHealth() {
    return 1
    // return this.healthCheckService.check([
    //   () => this.prismaService.isHealthy(),
    // ]);
  }
}
