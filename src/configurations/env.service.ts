import { Injectable, OnModuleInit } from "@nestjs/common";
import { PrismaService } from "src/database/prisma.service";
// import { Cron, Interval, Timeout, SchedulerRegistry } from '@nestjs/schedule';
@Injectable()
export class EnvService {
  private readonly envConfig: EnvConfig
  constructor(
  ) {
    this.envConfig = {
      NODE_ENV: process.env.NODE_ENV,
      PORT: parseInt(process.env.PORT, 10) || 3000,

      REDIS: {
        HOST: process.env.REDIS_HOST || 'localhost',
        PORT: parseInt(process.env.REDIS_PORT, 10) || 6379,
        PASSWORD: process.env.REDIS_PASSWORD || '',
        DB: parseInt(process.env.REDIS_DB, 10) || 0,
        PREFIX: process.env.REDIS_PREFIX || 'song_service',
      },
      
      RABBITMQ: {
        URL: process.env.RABBITMQ_URL || 'amqp://guest:guest@localhost:5672',
        SERVICE: {
          AUTH: process.env.RABBITMQ_AUTH_QUEUE || 'default-auth-queue',
          SONG: process.env.POST_QUEUE || 'default-song-queue',
        },
        QUEUE: {
          CONSUMER_SAVE_LOG_DB: process.env.SAVE_LOG_DB_CONSUMER || 'consumer_save_log_db',
        },
      },
      server_config: {}
    }
  }
  public get<T>(key: string, defaultValue?: any): T | undefined {
    const keys = key.split('.');
    let result: any = this.envConfig;
    for (const key of keys) {
      if (result && result.hasOwnProperty(key)) {
        result = result[key];
      } else {
        return defaultValue; // Trả về undefined nếu không tìm thấy khóa
      }
    }
    return result;
  }
}

interface EnvConfig {
  NODE_ENV: string | undefined;
  PORT: number;
  REDIS: {
    HOST: string;
    PORT: number;
    PASSWORD?: string;
    DB?: number;
    PREFIX?: string;
  };
  RABBITMQ: {
    URL: string;
    SERVICE: {
      AUTH: string;
      SONG: string;
    };
    QUEUE: {
      CONSUMER_SAVE_LOG_DB: string;
    };
  };
  server_config: any
}
