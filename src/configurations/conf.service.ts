import { Inject, Injectable, OnModuleInit } from "@nestjs/common";
import { PrismaService } from "src/database/prisma.service";
import * as NodeCache from 'node-cache';
@Injectable()
export class ConfService implements OnModuleInit {
  private myCache = new NodeCache();
  constructor(
    private readonly prisma: PrismaService,
  ) {
  }
  async onModuleInit() {
    try {
      const confs = await this.prisma.conf.findMany({
        select: {
          name: true,
          value: true
        }
      });
      let configs = confs.map(i => {
        return {
          key: i.name,
          val: i.value,
          // type: true
        }
      })
      this.myCache.mset(configs)
    } catch (error) {
      console.error('Error calling database function:', error);
    }
  }
  public get<T extends string | number>(key: keyof IConf, defaultValue?: T): T {
    let result = this.myCache.get(key);
    if (result === undefined && defaultValue !== undefined) {
      return defaultValue;
    }
    return result as T;
  }

  public async getJwtConfigs(defaultValue: IKeyValue = { JWT_SECRET: 'defaultSecret', JWT_EXPIRE: '70d' }): Promise<IKeyValue> {
    let jwts = await this.prisma.conf.findMany({
      where: {
        name: {
          startsWith: 'JWT_',
        },
      },
      select: {
        name: true,
        value: true
      },
    })
    if (jwts) {
      let jwtConfigs: IKeyValue = jwts.reduce((obj, item) => {
        obj[item.name] = item.value;
        return obj;
      }, {});
      return jwtConfigs;
    } else {
      return defaultValue;
    }
  }
}

interface IConf {
  JWT_SECRET: string,
  JWT_EXPIRE: string,
  KEY_DEV: string,
}
interface IKeyValue {
  [key: string]: any
}
