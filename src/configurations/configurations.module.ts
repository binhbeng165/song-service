import { Global, Module } from '@nestjs/common';
import { EnvService } from './env.service';
import { PrismaModule } from 'src/database/prisma.module';
import { ConfService } from './conf.service';

@Global()
@Module({
  imports: [PrismaModule],
  providers: [EnvService, ConfService],
  exports: [EnvService, ConfService],
})
export class ConfigurationModule { }
