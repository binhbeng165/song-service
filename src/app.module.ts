import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { join } from 'path';
import { APP_GUARD } from '@nestjs/core';
import { TerminusModule } from '@nestjs/terminus';
// import { AcceptLanguageResolver, I18nModule, QueryResolver } from 'nestjs-i18n';
import { PassportModule } from '@nestjs/passport';
import { AppController } from './app.controller';
import { EnvService } from 'src/configurations/env.service';
import { PrismaService } from 'src/database/prisma.service';
import { ConfigurationModule } from './configurations/configurations.module';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { ConfService } from './configurations/conf.service';
import { TracingMiddleware } from './core/middlewares/trace.middleware';
import { LogService } from './logger/logger.service';
import { RabbitMQModule } from './rabbitmq/rabbitmq.module';
import { BandModule } from './modules/band/band.module';
import { SongModule } from './modules/song/song.module';
import { ArtistModule } from './modules/artist/artist.module';
import { RequestlogModule } from './modules/requestlog/requestlog.module';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { ArtistController } from './modules/artist/artist.controller';
@Module({
  imports: [
    ConfigurationModule,
    RabbitMQModule,
    PassportModule.register({ defaultStrategy: 'jwt' }),
    // I18nModule.forRoot({
    //   fallbackLanguage: 'en',
    //   loaderOptions: {
    //     path: join(__dirname, '/i18n/'),
    //     watch: true,
    //   },
    //   resolvers: [
    //     { use: QueryResolver, options: ['lang'] },
    //     AcceptLanguageResolver,
    //   ],
    // }),
    TerminusModule,
    RequestlogModule,
    BandModule,
    SongModule,
    ArtistModule,
  ],
  controllers: [AppController],
  providers: [
    // JwtService,
    // EnvService,
    PrismaService,
    LogService,
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    // consumer.apply(TracingMiddleware).forRoutes('*');
    consumer.apply(TracingMiddleware).forRoutes({ path: 'test', method: RequestMethod.GET });
  }
}
