import {
  ClassSerializerInterceptor,
  Logger,
  ValidationPipe,
} from '@nestjs/common';
import { NestFactory, Reflector } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { ResponseInterceptor } from './core/interceptor/response.interceptor';
import { EnvService } from './configurations/env.service';
import { ExpressAdapter } from '@nestjs/platform-express';
import * as express from 'express';
import helmet from 'helmet';
import { UncaughtExceptionFilter, HttpExceptionFilter } from './core/filters';
// import * as apm from 'elastic-apm-node';
import { LogService } from './logger/logger.service';
import { RabbitMQService } from './rabbitmq/rabbitmq.service';

function configureSwagger(app): void {
  const config = new DocumentBuilder()
    .setTitle('auth-service')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('/api/docs', app, document);
}

async function bootstrap() {
  const logger = new Logger();
  const app = await NestFactory.create(
    AppModule,
    new ExpressAdapter(express()),
    {
      bufferLogs: true,
      cors: true,
    },
  );
  app.setGlobalPrefix('/api');
  app.use(helmet());

  // apm.start({
  //   serviceName: 'song_service',
  //   secretToken: 'X2vkRYpPDZdKCdo6DZ',
  //   serverUrl: 'https://c866e20e1d2d42d3974799af0e351965.apm.us-east-2.aws.elastic-cloud.com:443',
  //   environment: 'my-environment'
  // })
  const envConfigService = app.get(EnvService);
  const RmqService = app.get(RabbitMQService);
  const nodeEnv = envConfigService.get<string>('NODE_ENV');
  if (!nodeEnv) return 1;
  logger.log(
    `🚀 Ứng dụng đang chạy trong môi trường: ${nodeEnv}`,
  );
  const moduleRef = app.select(AppModule);
  const reflector = moduleRef.get(Reflector);
  app.useGlobalInterceptors(
    new ResponseInterceptor(reflector, new LogService(RmqService)),
    new ClassSerializerInterceptor(reflector),
  );
  app.useGlobalFilters(new UncaughtExceptionFilter(), new HttpExceptionFilter());
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      forbidNonWhitelisted: true,
      transform: true,
    }),
  );
  configureSwagger(app);
  app.connectMicroservice({
    transport: Transport.RMQ,
    options: {
      urls: [`${envConfigService.get('RABBITMQ.URL')}`],
      queue: `${envConfigService.get('RABBITMQ.SERVICE.SONG')}`,
      queueOptions: { durable: false },
      prefetchCount: 1,
    },
  });
  await app.startAllMicroservices();
  await app.listen(envConfigService.get('PORT'));
  logger.log(
    `🚀 Song service started successfully on port ${envConfigService.get(
      'PORT',
    )}`,
  );
}
bootstrap();
