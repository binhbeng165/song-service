import { Provider } from "@nestjs/common"
import amqpConnectionManager, {
  AmqpConnectionManager,
  Channel,
  ChannelWrapper,
} from 'amqp-connection-manager'
import { EnvService } from "src/configurations/env.service";

let uri = new EnvService().get('RABBITMQ.URL', 'amqps://tudsvftg:yMDD6a_PMobgi9rM3264RC3aBSsNE_a3@prawn.rmq.cloudamqp.com/tudsvftg')
const amqpConnection = amqpConnectionManager.connect(uri);

export const amqpConnectionProvider: Provider = {
  provide: 'amqpConnectionName',
  useFactory: (): AmqpConnectionManager => {
    amqpConnection.on('connect', () => {
      console.log(`Connected to RabbitMQ broker.`)
    })

    amqpConnection.on('disconnect', ({ err }) => {
      console.log(`Lost connection to RabbitMQ broker.\n${err.stack}`)
    })

    amqpConnection.on('error', ({ err }) => {
      console.log(`Error on connection to RabbitMQ broker.\n${err.stack}`)
    })

    return amqpConnection
  },
}
export const FirstChannel: Provider = {
  provide: 'first_channel',
  useFactory: async (): Promise<ChannelWrapper> => {
    const ch = amqpConnection.createChannel({
      setup: (ch: Channel) => {
        ch.on('error', (e: any) => {
          console.log('Channel error:', e)
        })
        console.log('AMQP pub connected')
      }
    })
    return ch
  },
}

export const amqpSubChannelProvider: Provider = {
  provide: 'amqpSubChannelName',
  useFactory: (): ChannelWrapper => {
    return amqpConnection.createChannel({
      setup: (ch: Channel) => {
        ch.on('error', (e: any) => {
          console.log('Channel error:', e)
        })
        console.log('AMQP sub connected')
        ch.prefetch(2)
      },
    })
  },
}