import { Global, Module, OnApplicationBootstrap, OnApplicationShutdown, Provider } from '@nestjs/common';
import { RabbitMQService } from './rabbitmq.service';
import { Channel, ChannelWrapper } from 'amqp-connection-manager';
import { FirstChannel } from './rabbitmq.provider';
import { ModuleRef } from '@nestjs/core';
import { Message } from 'amqplib';
import { DELAY_EXCHANGE, WRITE_LOG_EXCHANGE, createDeadLetterExchange, createDelayExchange, createWriteLogExchange, moveMessageToDelayExchange } from './rabbitmq.setup';
import { RequestlogService } from 'src/modules/requestlog/requestlog.service';
import { RequestlogModule } from 'src/modules/requestlog/requestlog.module';
@Global()
@Module({
  controllers: [],
  providers: [FirstChannel, RabbitMQService], //?? không có RabbitMQService thì không chạy ???
  imports: [RequestlogModule],
  exports: [RabbitMQService]
})
export class RabbitMQModule implements OnApplicationBootstrap, OnApplicationShutdown {
  constructor(
    private readonly moduleRef: ModuleRef,
    private readonly requestLogService: RequestlogService,
  ) { }

  async onApplicationBootstrap() {
    let channel = this.moduleRef.get<ChannelWrapper>('first_channel')
    await channel.addSetup(async (channel: Channel) => {

      await createWriteLogExchange(channel)
      await createDelayExchange(channel)
      await createDeadLetterExchange(channel)

      let writeLogConsumers = WRITE_LOG_EXCHANGE.queues
      for (const consumer of writeLogConsumers) {
        await channel.consume(consumer.name, async (msg) => {
          try {
            await this.requestLogService.addLog(JSON.parse(msg.content.toString()))
            channel.ack(msg)
          }
          catch (err) {
            moveMessageToDelayExchange(channel, msg);
            console.log("Server is down. Message is moved to delay exchange.")
          }
        }, { noAck: false });
      }
    })


    // this is func exec consumer
  }
  async onApplicationShutdown(signal?: string) {

  }
}
