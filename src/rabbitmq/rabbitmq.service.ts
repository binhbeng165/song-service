import { Inject, Injectable } from '@nestjs/common';
// import { ListExchangeName } from 'src/contanst/exchange.list';
// import { amqpPubChannelProvider } from './rabbitmq.provider';
import { Channel } from 'amqp-connection-manager';
@Injectable()
export class RabbitMQService {
    constructor(@Inject('first_channel') private amqpChannel: Channel) { }
    async publishMessage(exchange: string, routingKey: string, message: string): Promise<boolean> {
        return this.amqpChannel.publish(exchange, routingKey, Buffer.from(message))
    }
}