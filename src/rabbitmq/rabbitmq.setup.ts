import { Channel } from 'amqp-connection-manager';
import { Options } from 'amqplib';

export const MAX_RETRIES = 2
export const DELAY_DEFAULT = 6000

interface RabbitMQSetup {
  name: string;
  type: string;
  options: Options.AssertExchange
  queues: Queue[];
}

interface Queue {
  name: string;
  binding: string;
  options: Options.AssertQueue
}

export const DEAD_LETTER_EXCHANGE: RabbitMQSetup = Object.freeze({
  name: 'dead.letter.exchange',
  type: 'direct',
  options: {
    durable: true,
  },
  queues: [
    {
      name: 'dead.letter.queue', // subscribe to this queue
      binding: 'dead.letter.routing',
      options: {
        durable: true,
      },
    },
  ]
});

export const WRITE_LOG_EXCHANGE: RabbitMQSetup = Object.freeze({
  name: 'write.log.exchange',
  type: 'direct',
  options: {
    durable: true,
  },
  queues: [
    {
      name: 'create.log.db.queue', // subscribe to this queue
      binding: 'logs',
      options: {
        durable: true,
        deadLetterExchange: DEAD_LETTER_EXCHANGE.name,
        deadLetterRoutingKey: DEAD_LETTER_EXCHANGE.queues[0].binding,
      },
    },
    // {
    //   name: 'create_log_db_queue_1', // subscribe to this queue
    //   binding: 'logs',
    //   options: {
    //     durable: true,
    //     deadLetterExchange: DEAD_LETTER_EXCHANGE.queues[0].name,
    //     deadLetterRoutingKey: DEAD_LETTER_EXCHANGE.queues[0].binding,
    //   },
    // },
  ]
});
export const DELAY_EXCHANGE: RabbitMQSetup = Object.freeze({
  name: 'delay.exchange',
  type: 'direct',
  options: {
    durable: true,
  },
  queues: [
    {
      name: 'delay.queue', // subscribe to this queue
      binding: 'delay.routing',
      options: {
        durable: true,
        deadLetterExchange: WRITE_LOG_EXCHANGE.name,
        deadLetterRoutingKey: "logs",
        // messageTtl: 10 * 1000,
      }
    }

  ]
});

export const getDelayRetryInterval = (retryCount: number, defaultDelay: number): number => {
  const interval = Math.pow(10, retryCount) * defaultDelay
  console.log(`Next message will be retried after ${interval / 1000} sec`)
  return interval
}

export const createWriteLogExchange = async (channel: Channel) => {
  await channel.assertExchange(WRITE_LOG_EXCHANGE.name, WRITE_LOG_EXCHANGE.type, { durable: true });
  for (const queue of WRITE_LOG_EXCHANGE.queues) {
    await channel.assertQueue(queue.name, queue.options);
    await channel.bindQueue(queue.name, WRITE_LOG_EXCHANGE.name, queue.binding);
  }
}

export const createDelayExchange = async (channel: Channel) => {
  await channel.assertExchange(DELAY_EXCHANGE.name, DELAY_EXCHANGE.type, { durable: true });
  for (const queue of DELAY_EXCHANGE.queues) {
    await channel.assertQueue(queue.name, queue.options);
    await channel.bindQueue(queue.name, DELAY_EXCHANGE.name, queue.binding);
  }
}

export const createDeadLetterExchange = async (channel: Channel) => {
  await channel.assertExchange(DEAD_LETTER_EXCHANGE.name, DEAD_LETTER_EXCHANGE.type, { durable: true });
  for (const queue of DEAD_LETTER_EXCHANGE.queues) {
    await channel.assertQueue(queue.name, queue.options);
    await channel.bindQueue(queue.name, DEAD_LETTER_EXCHANGE.name, queue.binding);
  }
}

export const moveMessageToDelayExchange = (channel: Channel, msg: any) => {
  let retryCount = msg.properties.headers['x-retry-count'] || 0
  if (retryCount >= MAX_RETRIES) {
    channel.nack(msg, false, false);
    console.log(`Max retries exceeded`);
    return
  }
  channel.ack(msg)
  let newDelay = getDelayRetryInterval(retryCount, DELAY_DEFAULT)
  channel.publish(DELAY_EXCHANGE.name, DELAY_EXCHANGE.queues[0].binding, msg.content, {
    headers: {
      'x-retry-count': retryCount + 1,
    },
    expiration: newDelay
  })
}