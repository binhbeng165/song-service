import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import * as apm from 'elastic-apm-node';
@Injectable()
export class TracingMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: NextFunction) {
    console.log(`Request in middle ware: ${req.method} ${req.baseUrl}`);
    // const transaction = apm.startTransaction(`${req.baseUrl}`, 'request');
    // req['transaction'] = transaction;
    // res.on("finish", () => {
    //   const { statusCode } = res;
    //   console.log(
    //     `${statusCode} hihihihehe`,
    //   );
    // });
    next();
  }
}