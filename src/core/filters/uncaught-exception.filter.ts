import { ArgumentsHost, Catch, ContextType, ExceptionFilter, HttpStatus } from '@nestjs/common';
import { Response } from 'express';
import { EMPTY } from 'rxjs';

const ignoredHttpStatuses = [
    HttpStatus.NOT_FOUND,
    HttpStatus.FORBIDDEN,
    HttpStatus.METHOD_NOT_ALLOWED,
    HttpStatus.PAYLOAD_TOO_LARGE
];

@Catch()
export class UncaughtExceptionFilter implements ExceptionFilter {
    catch(exception: any, host: ArgumentsHost) {

        // get the original exception if it was caught more than once
        if (exception.status === HttpStatus.FORBIDDEN) {
            exception.status = HttpStatus.METHOD_NOT_ALLOWED;
        }
        // // determine the context type
        let contextType = 'rpc'
        const res = host.switchToHttp().getResponse();
        if (res.req && res.status && typeof res.status === 'function') {
            contextType = 'http';
        }
        // if http, then form response
        if (contextType === 'http') {
            // handle stack traces
            if (ignoredHttpStatuses.indexOf(exception.status) === -1) {
                // this.errorHandler.captureException(exception);
            }
            const res: Response = host.switchToHttp().getResponse();

            const statusCode = exception.status || 500;

            let message: any;
            if (exception.message) {
                message = exception.message.error || exception.message;
            } else {
                message = 'There was an internal server error';
            }

            const exceptionResponse = {
                statusCode,
                appCode: exception.appCode ?? HttpStatus[statusCode],
                message,
                ...exception.customResponse
            };

            res.status(statusCode).json(exceptionResponse);
        } else {
            return handleRpcException(exception, host, true);
        }
    }
}
function handleRpcException(exception: any, host: ArgumentsHost, logException: boolean) {

    return EMPTY;
}

