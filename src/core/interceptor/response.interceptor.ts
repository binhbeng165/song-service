import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable, firstValueFrom, map, of } from 'rxjs';
import { LogService } from 'src/logger/logger.service';

@Injectable()
export class ResponseInterceptor implements NestInterceptor {
  statusMessages: { [key: string]: string };
  public constructor(
    private readonly reflector: Reflector,
    private readonly loggerService: LogService) {
    this.statusMessages = {
      200: 'OK',
      201: 'Created',
      202: 'Accepted',
      203: 'NonAuthoritativeInfo',
      204: 'NoContent',
      205: 'ResetContent',
      206: 'PartialContent',
    };
  }

  public intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Observable<any> {
    let start = new Date().getTime()
    const request = context.switchToHttp().getRequest();
    console.log('haha', new Date().getTime())
    const status = this.reflector.get<number>('__httpCode__', context.getHandler()) || 200;
    // return of({
    //   statusCode: status,
    //   message: this.statusMessages[status],
    //   data: body,
    // });
    // sau next.handle() la response
    return next.handle().pipe(
      map((data) => {
        this.loggerService.logRequest({
          url: request.url,
          method: request.method,
          req: Object.keys(request.body).length !== 0 ? request.body : null,
          res: data,
          headers: JSON.stringify(request.headers),
          statusCode: status,
          message: this.statusMessages[status],
          dur: new Date().getTime() - start
        })
        return {
          statusCode: status,
          message: this.statusMessages[status],
          data,
        };
      }),
    );

  }
}
