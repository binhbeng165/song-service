import { Injectable } from '@nestjs/common';
import * as winston from 'winston';
import { ecsFormat } from '@elastic/ecs-winston-format';
import { RabbitMQService } from 'src/rabbitmq/rabbitmq.service';
import * as _ from "lodash"
import { WRITE_LOG_EXCHANGE } from 'src/rabbitmq/rabbitmq.setup';
@Injectable()
export class LogService {
  private readonly logger: winston.Logger;

  constructor(
    private readonly rabbitMq: RabbitMQService
  ) {

    const sensitiveFields: string[] = ['req.headers.authorization', 'req.password'];

    const redactSensitiveInfo = winston.format((info: any, opts) => {
      for (const field of sensitiveFields) {
        const currentValue = _.get(info, field);
        if (currentValue) {
          _.set(info, field, 'sensitive');
        }
      }
      return info;
    });
    this.logger = winston.createLogger({
      level: 'info',
      format: winston.format.combine(
        ecsFormat(),
        redactSensitiveInfo()
      ),
      // format: winston.format.json(),
      defaultMeta: { service: 'auth-service' },
      transports: [
        new winston.transports.File({ filename: 'combined.log' }),
      ],
    });
  }
  logRequest(data: any) {
    this.logger.info(data);
    this.rabbitMq.publishMessage(WRITE_LOG_EXCHANGE.name, WRITE_LOG_EXCHANGE.queues[0].binding, JSON.stringify(data))
  }
}
