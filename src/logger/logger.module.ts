import { Module } from '@nestjs/common';
import { PrismaModule } from 'src/database/prisma.module';
import { LogService } from './logger.service';
import { RabbitMQModule } from 'src/rabbitmq/rabbitmq.module';

@Module({
  imports: [
    RabbitMQModule
  ],
  controllers: [],
  providers: [LogService],
})
export class LogModule { }
