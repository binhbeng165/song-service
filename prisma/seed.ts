import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient()
async function main() {
    await prisma.song.create({
        data: {
            name: 'Xe đạp',
            releaseYear: "2006",
            lyrics: "Dường như nắng đã làm",
            composers: {
                create: [
                    { name: 'Đinh Mạnh Ninh', bio: 'Sync with your schema' },
                    { name: 'Đinh Mạnh Minh', bio: 'hehehe' },
                ],
            },
            artists: {
                create: [
                    { name: 'Thùy Chi', bio: "ARTIST", },
                ],
            },
            bands: {
                create: [
                    { name: 'M4U', artistIds: [1, 2] },
                ]
            }
        },
    })

}

main().then(() => { console.log('Seeding completed') }).catch(error => {
    console.log("Seeding err: ", error)
})