-- CreateEnum
CREATE TYPE "PerformerType" AS ENUM ('SOLO', 'VARIOUS', 'BAND');

-- CreateTable
CREATE TABLE "artist" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "bio" TEXT NOT NULL,
    "createdAt" TIMESTAMPTZ(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "deletedAt" TIMESTAMP(3),
    "isDeleted" BOOLEAN,

    CONSTRAINT "artist_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "song" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "lyrics" TEXT NOT NULL,
    "image" TEXT,
    "releaseYear" TEXT NOT NULL,
    "chords" TEXT[],
    "createdAt" TIMESTAMPTZ(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "deletedAt" TIMESTAMP(3),
    "isDeleted" BOOLEAN,

    CONSTRAINT "song_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "band" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "artirstIds" INTEGER[],

    CONSTRAINT "band_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "perform" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "songId" INTEGER NOT NULL,
    "refType" "PerformerType" NOT NULL,
    "refId" INTEGER NOT NULL,

    CONSTRAINT "perform_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "conf" (
    "id" SERIAL NOT NULL,
    "name" TEXT,
    "value" TEXT,
    "type" TEXT,
    "createdAt" TIMESTAMPTZ(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMPTZ(3) NOT NULL,
    "deletedAt" TIMESTAMP(3),

    CONSTRAINT "conf_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "requestlog" (
    "id" SERIAL NOT NULL,
    "url" TEXT,
    "method" TEXT,
    "statusCode" INTEGER,
    "req" TEXT,
    "res" TEXT,
    "dur" INTEGER,
    "headers" TEXT,
    "timestamp" TEXT,
    "createdAt" TIMESTAMPTZ(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMPTZ(3) NOT NULL,
    "deletedAt" TIMESTAMP(3),

    CONSTRAINT "requestlog_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "_ArtistToSong" (
    "A" INTEGER NOT NULL,
    "B" INTEGER NOT NULL
);

-- CreateTable
CREATE TABLE "_PerformToSong" (
    "A" INTEGER NOT NULL,
    "B" INTEGER NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "_ArtistToSong_AB_unique" ON "_ArtistToSong"("A", "B");

-- CreateIndex
CREATE INDEX "_ArtistToSong_B_index" ON "_ArtistToSong"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_PerformToSong_AB_unique" ON "_PerformToSong"("A", "B");

-- CreateIndex
CREATE INDEX "_PerformToSong_B_index" ON "_PerformToSong"("B");

-- AddForeignKey
ALTER TABLE "_ArtistToSong" ADD CONSTRAINT "_ArtistToSong_A_fkey" FOREIGN KEY ("A") REFERENCES "artist"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_ArtistToSong" ADD CONSTRAINT "_ArtistToSong_B_fkey" FOREIGN KEY ("B") REFERENCES "song"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_PerformToSong" ADD CONSTRAINT "_PerformToSong_A_fkey" FOREIGN KEY ("A") REFERENCES "perform"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_PerformToSong" ADD CONSTRAINT "_PerformToSong_B_fkey" FOREIGN KEY ("B") REFERENCES "song"("id") ON DELETE CASCADE ON UPDATE CASCADE;
