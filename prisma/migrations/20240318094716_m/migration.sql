/*
  Warnings:

  - You are about to drop the `_ComposerToArtist` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "_ComposerToArtist" DROP CONSTRAINT "_ComposerToArtist_A_fkey";

-- DropForeignKey
ALTER TABLE "_ComposerToArtist" DROP CONSTRAINT "_ComposerToArtist_B_fkey";

-- DropTable
DROP TABLE "_ComposerToArtist";

-- CreateTable
CREATE TABLE "_ComposerToSong" (
    "A" INTEGER NOT NULL,
    "B" INTEGER NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "_ComposerToSong_AB_unique" ON "_ComposerToSong"("A", "B");

-- CreateIndex
CREATE INDEX "_ComposerToSong_B_index" ON "_ComposerToSong"("B");

-- AddForeignKey
ALTER TABLE "_ComposerToSong" ADD CONSTRAINT "_ComposerToSong_A_fkey" FOREIGN KEY ("A") REFERENCES "artist"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_ComposerToSong" ADD CONSTRAINT "_ComposerToSong_B_fkey" FOREIGN KEY ("B") REFERENCES "song"("id") ON DELETE CASCADE ON UPDATE CASCADE;
