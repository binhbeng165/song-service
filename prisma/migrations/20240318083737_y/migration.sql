/*
  Warnings:

  - You are about to drop the column `artirstIds` on the `band` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "band" DROP COLUMN "artirstIds",
ADD COLUMN     "artistIds" INTEGER[];
