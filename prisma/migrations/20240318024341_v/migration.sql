/*
  Warnings:

  - The values [SOLO,VARIOUS] on the enum `PerformerType` will be removed. If these variants are still used in the database, this will fail.

*/
-- AlterEnum
BEGIN;
CREATE TYPE "PerformerType_new" AS ENUM ('ARTIST', 'BAND');
ALTER TABLE "performer" ALTER COLUMN "refType" TYPE "PerformerType_new" USING ("refType"::text::"PerformerType_new");
ALTER TYPE "PerformerType" RENAME TO "PerformerType_old";
ALTER TYPE "PerformerType_new" RENAME TO "PerformerType";
DROP TYPE "PerformerType_old";
COMMIT;
