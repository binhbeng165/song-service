/*
  Warnings:

  - The values [SOLO,VARIOUS] on the enum `PerformerType` will be removed. If these variants are still used in the database, this will fail.
  - You are about to drop the `_PerformToSong` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `perform` table. If the table is not empty, all the data it contains will be lost.

*/
-- AlterEnum


-- DropForeignKey
ALTER TABLE "_PerformToSong" DROP CONSTRAINT "_PerformToSong_A_fkey";

-- DropForeignKey
ALTER TABLE "_PerformToSong" DROP CONSTRAINT "_PerformToSong_B_fkey";

-- DropTable
DROP TABLE "_PerformToSong";

-- DropTable
DROP TABLE "perform";

-- CreateTable
CREATE TABLE "performer" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "songId" INTEGER NOT NULL,
    "refType" "PerformerType" NOT NULL,
    "refId" INTEGER NOT NULL,

    CONSTRAINT "performer_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "_PerformerToSong" (
    "A" INTEGER NOT NULL,
    "B" INTEGER NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "_PerformerToSong_AB_unique" ON "_PerformerToSong"("A", "B");

-- CreateIndex
CREATE INDEX "_PerformerToSong_B_index" ON "_PerformerToSong"("B");

-- AddForeignKey
ALTER TABLE "_PerformerToSong" ADD CONSTRAINT "_PerformerToSong_A_fkey" FOREIGN KEY ("A") REFERENCES "performer"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_PerformerToSong" ADD CONSTRAINT "_PerformerToSong_B_fkey" FOREIGN KEY ("B") REFERENCES "song"("id") ON DELETE CASCADE ON UPDATE CASCADE;
