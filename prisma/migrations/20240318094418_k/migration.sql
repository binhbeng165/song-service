/*
  Warnings:

  - You are about to drop the `_SongArtists` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `_SongBands` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `_SongComposers` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "_SongArtists" DROP CONSTRAINT "_SongArtists_A_fkey";

-- DropForeignKey
ALTER TABLE "_SongArtists" DROP CONSTRAINT "_SongArtists_B_fkey";

-- DropForeignKey
ALTER TABLE "_SongBands" DROP CONSTRAINT "_SongBands_A_fkey";

-- DropForeignKey
ALTER TABLE "_SongBands" DROP CONSTRAINT "_SongBands_B_fkey";

-- DropForeignKey
ALTER TABLE "_SongComposers" DROP CONSTRAINT "_SongComposers_A_fkey";

-- DropForeignKey
ALTER TABLE "_SongComposers" DROP CONSTRAINT "_SongComposers_B_fkey";

-- DropTable
DROP TABLE "_SongArtists";

-- DropTable
DROP TABLE "_SongBands";

-- DropTable
DROP TABLE "_SongComposers";

-- CreateTable
CREATE TABLE "_ComposerToArtist" (
    "A" INTEGER NOT NULL,
    "B" INTEGER NOT NULL
);

-- CreateTable
CREATE TABLE "_ArtistToSong" (
    "A" INTEGER NOT NULL,
    "B" INTEGER NOT NULL
);

-- CreateTable
CREATE TABLE "_BandToSong" (
    "A" INTEGER NOT NULL,
    "B" INTEGER NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "_ComposerToArtist_AB_unique" ON "_ComposerToArtist"("A", "B");

-- CreateIndex
CREATE INDEX "_ComposerToArtist_B_index" ON "_ComposerToArtist"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_ArtistToSong_AB_unique" ON "_ArtistToSong"("A", "B");

-- CreateIndex
CREATE INDEX "_ArtistToSong_B_index" ON "_ArtistToSong"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_BandToSong_AB_unique" ON "_BandToSong"("A", "B");

-- CreateIndex
CREATE INDEX "_BandToSong_B_index" ON "_BandToSong"("B");

-- AddForeignKey
ALTER TABLE "_ComposerToArtist" ADD CONSTRAINT "_ComposerToArtist_A_fkey" FOREIGN KEY ("A") REFERENCES "artist"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_ComposerToArtist" ADD CONSTRAINT "_ComposerToArtist_B_fkey" FOREIGN KEY ("B") REFERENCES "song"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_ArtistToSong" ADD CONSTRAINT "_ArtistToSong_A_fkey" FOREIGN KEY ("A") REFERENCES "artist"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_ArtistToSong" ADD CONSTRAINT "_ArtistToSong_B_fkey" FOREIGN KEY ("B") REFERENCES "song"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_BandToSong" ADD CONSTRAINT "_BandToSong_A_fkey" FOREIGN KEY ("A") REFERENCES "band"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_BandToSong" ADD CONSTRAINT "_BandToSong_B_fkey" FOREIGN KEY ("B") REFERENCES "song"("id") ON DELETE CASCADE ON UPDATE CASCADE;
