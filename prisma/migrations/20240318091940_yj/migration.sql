/*
  Warnings:

  - You are about to drop the `_ArtistToSong` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `_PerformerToSong` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `performer` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "_ArtistToSong" DROP CONSTRAINT "_ArtistToSong_A_fkey";

-- DropForeignKey
ALTER TABLE "_ArtistToSong" DROP CONSTRAINT "_ArtistToSong_B_fkey";

-- DropForeignKey
ALTER TABLE "_PerformerToSong" DROP CONSTRAINT "_PerformerToSong_A_fkey";

-- DropForeignKey
ALTER TABLE "_PerformerToSong" DROP CONSTRAINT "_PerformerToSong_B_fkey";

-- DropTable
DROP TABLE "_ArtistToSong";

-- DropTable
DROP TABLE "_PerformerToSong";

-- DropTable
DROP TABLE "performer";

-- CreateTable
CREATE TABLE "_SongComposers" (
    "A" INTEGER NOT NULL,
    "B" INTEGER NOT NULL
);

-- CreateTable
CREATE TABLE "_SongArtists" (
    "A" INTEGER NOT NULL,
    "B" INTEGER NOT NULL
);

-- CreateTable
CREATE TABLE "_SongBands" (
    "A" INTEGER NOT NULL,
    "B" INTEGER NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "_SongComposers_AB_unique" ON "_SongComposers"("A", "B");

-- CreateIndex
CREATE INDEX "_SongComposers_B_index" ON "_SongComposers"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_SongArtists_AB_unique" ON "_SongArtists"("A", "B");

-- CreateIndex
CREATE INDEX "_SongArtists_B_index" ON "_SongArtists"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_SongBands_AB_unique" ON "_SongBands"("A", "B");

-- CreateIndex
CREATE INDEX "_SongBands_B_index" ON "_SongBands"("B");

-- AddForeignKey
ALTER TABLE "_SongComposers" ADD CONSTRAINT "_SongComposers_A_fkey" FOREIGN KEY ("A") REFERENCES "artist"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_SongComposers" ADD CONSTRAINT "_SongComposers_B_fkey" FOREIGN KEY ("B") REFERENCES "song"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_SongArtists" ADD CONSTRAINT "_SongArtists_A_fkey" FOREIGN KEY ("A") REFERENCES "artist"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_SongArtists" ADD CONSTRAINT "_SongArtists_B_fkey" FOREIGN KEY ("B") REFERENCES "song"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_SongBands" ADD CONSTRAINT "_SongBands_A_fkey" FOREIGN KEY ("A") REFERENCES "band"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_SongBands" ADD CONSTRAINT "_SongBands_B_fkey" FOREIGN KEY ("B") REFERENCES "song"("id") ON DELETE CASCADE ON UPDATE CASCADE;
